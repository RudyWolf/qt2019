#include "pikinv.h"
#include "ui_pikinv.h"
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>

#include <sha1.h>
#include <bmpinject.h>

PikInv::PikInv(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PikInv)
{
    ui->setupUi(this);

    // Автоматически открыть картинку при открытии, если это возможно
    QString img = qApp->applicationDirPath() + "/../24.bmp";
    if(QFile::exists(img))
    {
        QPixmap p = QPixmap(img);
        if(!p.isNull())
        {
            ui->imgSrc->setPixmap(p);
            m_lastPikPath = img;
        }
    }
}

PikInv::~PikInv()
{
    delete ui;
}

void PikInv::on_doSha1_clicked()
{
    // Формируем строку на хэширование
    QString str = "Языки программирования_" + ui->fio->text();

    // Заменим все пробелы на прочерки
    str.replace(' ', '_');

    // Преобразуем её в std::string
    std::string src = str.toStdString();

    // Выделяем приёмный буфер
    std::string sha1_buffer;
    sha1_buffer.resize(100, '\0');

    // Вызываем хэширование через нашу либу
    int len = wolf_makeSha1FromName(src.c_str(), &sha1_buffer[0], sha1_buffer.size());

    // Калибруем буфер под размер принятой строчки
    sha1_buffer.resize(static_cast<size_t>(len));

    // Заносим сгенерированный хэш в специальное поле
    ui->dstSha1->setText(QString::fromStdString(sha1_buffer));
}

void PikInv::on_actionOpenPik_triggered()
{
    // Открываем картинку
    QString t = QFileDialog::getOpenFileName(this, "Открыть!", qApp->applicationDirPath(), "BMP-Картинка по 24-му варианту (24.bmp);; Всё (*.*)");

    // Если картинка выбрана и файл существует
    if(!t.isEmpty() && QFile::exists(t))
    {
        // Запоминаем путь
        m_lastPikPath = t;

        // Загружаем картинку на экран в левую панель
        QPixmap p = QPixmap(t);
        ui->imgSrc->setPixmap(p);
    }
}

void PikInv::on_injectHash_clicked()
{
    // Если картинка была загружена и она существует - делаем наше грязное дело
    if(!m_lastPikPath.isEmpty() && QFile::exists(m_lastPikPath))
    {
        // Генерируем путь к картинке с данными
        QFileInfo ti = QFileInfo(m_lastPikPath);
        QString t2 = ti.absoluteDir().absolutePath() + "/" + ti.baseName() + "_with_data.bmp";

        // ЕСЛИ какой-то шлак остался
        if(QFile::exists(t2))
            QFile::remove(t2); // удалим его

        // Создаём копию исходной картинки
        QFile::copy(m_lastPikPath, t2);

        std::string t2_std = t2.toStdString();
        std::string hash_std = ui->dstSha1->text().toStdString();

        // И затем, в копию исходной картинки внедряем наш хэш
        const char *err = wolf_injectCode(t2_std.c_str(), hash_std.c_str());
        if(err) // Если что-то пошло не так, ругаемся, ааааа!!!
        {
            QMessageBox::warning(this, tr("Упс!"), QString("Вышла ошибочка: %s").arg(err));
            return;
        }

        // Ну а если всё ок, то считываем новую картинку с нуля
        QPixmap p = QPixmap(t2);
        if(p.isNull())
        {
            QMessageBox::warning(this, tr("Упс!"), QString("Вышла ошибочка: Что-то не так с картинкой, увы..."));
            return;
        }

        // И загружаем её на экран в правую панель
        ui->imgDst->setPixmap(p);
        // Ну, для того, чтобы удостовериться в том, что мы ничего не повредили и картинка такая же какакя и была

        // Ну а теперь попроуем считать код и проверить, всё ли верно или нет?

        // Для этого выделим буфер для хэша
        std::string hash_std_verify;
        hash_std_verify.resize(100, '\0');

        // Извлечь хэш из новой картинки, куда мы его записали совсем недавно
        int gotLen = wolf_extractCode(t2_std.c_str(), &hash_std_verify[0], hash_std_verify.size());
        if(gotLen <= 0) // Опять же, если что-то пошло не так - кричим, аааааа!!!
        {
            QMessageBox::warning(this, tr("Упс!"), QString("Вышла ошибочка: Не получилось считать код для проверки..."));
            return;
        }

        // Подгоняем буфер под размер данных
        hash_std_verify.resize(static_cast<size_t>(gotLen));

        // Сверяем исходный хэш, который был записан заранее и считанный хэш
        if(hash_std != hash_std_verify)
        {
            QMessageBox::warning(this, tr("Упс!"),
                                 QString("Вышла ошибочка: коды различаются!\n%1\n%2")
                                 .arg(QString::fromStdString(hash_std))
                                 .arg(QString::fromStdString(hash_std_verify)));
            return;
        }

        // Если всё записано и все проверки пройдены - дело в шляпе!
        QMessageBox::information(this, tr("Сделяль!"), QString("Код успешно внедрён!"));
    }
}
