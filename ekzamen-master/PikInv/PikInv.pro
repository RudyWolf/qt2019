#-------------------------------------------------
#
# Project created by QtCreator 2019-06-10T11:25:08
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PikInv
TEMPLATE = app

# Следующее определение заставляет ваш компилятор выдавать предупреждения, если вы используете
# любая особенность Qt, которая была помечена как устаревшая (точные предупреждения
# зависит от вашего компилятора). Пожалуйста, обратитесь к документации
# устарел API, чтобы знать, как переносить код из него.
DEFINES += QT_DEPRECATED_WARNINGS



# Вы также можете сделать так, чтобы ваш код не компилировался, если вы используете устаревшие API.
# Для этого раскомментируйте следующую строку.
# Вы также можете отключить устаревшие API только до определенной версии Qt.
#DEFINES + = QT_DISABLE_DEPRECATED_BEFORE = 0x060000 # отключает все API, устаревшие до Qt 6.0.0

DESTDIR = $$PWD/../bin

win32:{
    INCLUDEPATH += $$PWD/../openssl/win64/include
    LIBS += -L$$PWD/../openssl/win64/lib
#    copydata.commands = $(COPY_DIR) $$PWD/../openssl/win64/*.dll $$DESTDIR
#    first.depends = $(first) copydata
#    export(first.depends)
#    export(copydata.commands)
#    QMAKE_EXTRA_TARGETS += first copydata
}

INCLUDEPATH += $$PWD/../sha1/ $$PWD/../bmpinject/

LIBS += -L$$PWD/../libs/ -lwolfsha1 -lbmpinject -lcrypto

CONFIG += c++11

SOURCES += \
        main.cpp \
        pikinv.cpp

HEADERS += \
        pikinv.h

FORMS += \
        pikinv.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
