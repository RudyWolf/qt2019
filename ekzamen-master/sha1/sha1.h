#ifndef SHA1_H
#define SHA1_H

#include <stddef.h>

/**
  Заголовочный файл модуля для хэширования строки с названием дицсиплины и ФИО с помощью SHA1 и OpenSSL
*/

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @brief Захэшировать строку в SHA1
 * @param nameAndGroup Дисциплина_Фамилия Имя Отчество
 * @param output Выходной буфер для записи хэша SHA1
 * @param bufferSize Размер выходного буфера
 * @return Длина строки с полученным хэшом
 */
extern int wolf_makeSha1FromName(const char *nameAndGroup, char *output, size_t bufferSize);

#ifdef __cplusplus
}
#endif

#endif // SHA1_H
