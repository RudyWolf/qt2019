#-------------------------------------------------
#
# Project created by QtCreator 2019-06-10T11:43:04
#
#-------------------------------------------------

QT       -= core gui
CONFIG -= qt

DESTDIR = $$PWD/../libs

TARGET = wolfsha1
TEMPLATE = lib
CONFIG += staticlib

win32:{
    INCLUDEPATH += $$PWD/../openssl/win64/include
    LIBS += $$PWD/../openssl/win64/lib
}

LIBS += -lcrypto

SOURCES += \
    sha1.c

HEADERS += \
        sha1.h
