#ifndef BMPINJECT_H
#define BMPINJECT_H

#include <stddef.h>

/**
  Заголовочный файл модуля для внедрения и извлечения строки из BMP-файла по варианту номер 24
*/

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @brief Внедряет строку в указанный BMP-файл
 * @param pathToBmp Путь к файлу в UTF8-формате
 * @param hash Строка, которую надо записать
 * @return Сообщение об ошибке или NULL если всё прошло успешно
 */
extern const char *wolf_injectCode(const char *pathToBmp, const char *hash);

/**
 * @brief Извлечь строку из указанного BMP-файла
 * @param pathToBmp Путь к файлу в UTF8-формате
 * @param output Выходной буфер для строки, извлечённой из BMP-файла
 * @param outputSize Размер выходного буфера
 * @return Размер извлечённых данных из буфера
 */
extern int wolf_extractCode(const char *pathToBmp, char *output, size_t outputSize);

#ifdef __cplusplus
}
#endif


#endif // BMPINJECT_H
